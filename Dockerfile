FROM openjdk:8-alpine

COPY ./target/java-maven-junit-helloworld-2.0-SNAPSHOT.jar /app/java_app.jar
WORKDIR /app
RUN chmod 744 java_app.jar
ENTRYPOINT ["java","-jar","java_app.jar"]
